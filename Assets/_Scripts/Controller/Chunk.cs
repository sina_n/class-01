﻿using System;
using UnityEngine;

namespace _Scripts.Controller
{
    public class Chunk : MonoBehaviour
    {
        private void Update()
        {
            var speed = ChunkManager.Instance.ChunkMoveSpeed;
            transform.position += Vector3.left * speed * Time.deltaTime;

            if (transform.position.x < ChunkManager.Instance.leftBoundary.position.x)
            {
                ChunkManager.Instance.PlaceChunk(this);
            }
        }
    }
}