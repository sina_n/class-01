﻿using System.Collections.Generic;
using System.Linq;
using _Scripts.Utils;
using UnityEngine;

namespace _Scripts.Controller
{
    public class ChunkManager : MonoBehaviourSingleton<ChunkManager>
    {
        [SerializeField] private float chunkMoveSpeed;
        public float ChunkMoveSpeed => chunkMoveSpeed;

        [SerializeField] private List<Chunk> chunks;

        public Transform leftBoundary;
        public Transform rightBoundary;

        public void PlaceChunk(Chunk lastChunk)
        {
            lastChunk.gameObject.SetActive(false);
            var deactiveChunks = chunks.FindAll(x => !x.gameObject.activeInHierarchy);
            var r = Random.Range(0, deactiveChunks.Count);
            var chunk = deactiveChunks[r];
            chunk.gameObject.SetActive(true);
            chunk.transform.position = rightBoundary.position;
        }
    }
}