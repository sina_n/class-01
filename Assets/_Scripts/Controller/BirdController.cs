using System;
using UnityEngine;

namespace _Scripts.Controller
{
    public class BirdController : MonoBehaviour
    {
        [SerializeField] private Rigidbody rb;
        [SerializeField] private SphereCollider collider;
        [SerializeField] private float jumpPower;
        [SerializeField] private float radius; 
    
        private void Update()
        {
            if (Input.GetButtonDown("Jump"))
            {
                rb.AddForce(Vector3.up * jumpPower);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            var f1 = 5;
            var f2 = 5.1;

            if (other.gameObject.CompareTag("Obstacle"))
            {
                print("game over");
            }
        }
    }
}
